This app packages FreshRSS 1.6.2.

FreshRSS is a self-hosted RSS feed aggregator such as Leed or Kriss Feed.

It is at the same time lightweight, easy to work with, powerful and customizable.

It is a multi-user application with an anonymous reading mode. It supports PubSubHubbub for instant notifications from compatible Web sites. There is an API for (mobile) clients, and a Command-Line Interface. Finally, it supports extensions for further tuning.

### Extensions 
FreshRSS supports further customizations by adding extensions on top of its core functionality.
See the [repository dedicated to those extensions](https://github.com/FreshRSS/Extensions). 


### Compatible clients
Any client supporting a Google Reader-like API. Selection:

* Android
	* [News+](https://play.google.com/store/apps/details?id=com.noinnion.android.newsplus) with [News+ Google Reader extension](https://play.google.com/store/apps/details?id=com.noinnion.android.newsplus.extension.google_reader) (Closed source)
	* [EasyRSS](https://github.com/Alkarex/EasyRSS) (Open source, F-Droid)
* Linux
	* [FeedReader 2.0+](https://jangernert.github.io/FeedReader/) (Open source)
