#!/bin/bash

set -eu

mkdir -p /run/php/sessions

if ! [ -f /app/data/.installed ]; then
  echo "Fresh installation, setting up..."
  rsync -a /app/code/data-orig/ /app/data/
  php cli/do-install.php \
    --environment production --default_user admin \
    --db-type mysql --db-host "${MYSQL_HOST}:${MYSQL_PORT}" \
    --db-user "${MYSQL_USERNAME}" --db-password "${MYSQL_PASSWORD}" \
    --db-base "${MYSQL_DATABASE}" --db-prefix "" \
    --disable_update
  php cli/create-user.php --user admin --password password --language en
  php cli/actualize-user.php --user admin
  touch /app/data/.installed
  echo "Done."
fi

mkdir -p /app/data/extensions
for f in $(ls /app/code/extensions-orig); do
  if ! [ -e "/app/data/extensions/$f" ]; then
    ln -s "/app/code/extensions-orig/$f" "/app/data/extensions/$f"
  fi
done


echo "Updating config file"
php cli/reconfigure.php --default_user admin --base_url "https://${APP_DOMAIN}/p" \
    --db-type mysql --db-host "${MYSQL_HOST}:${MYSQL_PORT}" \
    --db-user "${MYSQL_USERNAME}" --db-password "${MYSQL_PASSWORD}" \
    --db-base "${MYSQL_DATABASE}" --db-prefix "" \
    --disable_update


echo "Setting permissions"
chown -R www-data.www-data /run/php /app/data


echo "Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND
