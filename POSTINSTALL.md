This application does not integrate with Cloudron authentication.

There is a preconfigured administrator account with the following credentials:

* Username: `admin`
* Password: `password`

(Please change that password on first login)

You can create more accounts withing the app when logged in as administrator.
