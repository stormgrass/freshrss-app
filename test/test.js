#!/usr/bin/env node

'use strict';

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    webdriver = require('selenium-webdriver');

var by = webdriver.By,
    until = webdriver.until;

var username = 'admin',
    password = 'password';


process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';


describe('Application life cycle test', function () {
    this.timeout(0);

    var chrome = require('selenium-webdriver/chrome');
    var server, browser = new chrome.Driver();

    before(function (done) {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    var LOCATION = 'freshrss-test';
    var TEST_TIMEOUT = 10000;
    var app;

    function exists(selector, callback) {
        return browser.wait(until.elementLocated(selector), TEST_TIMEOUT).then(function () {
            callback();
        });
    }

    function visible(selector, callback) {
        exists(selector, function () {
            browser.wait(until.elementIsVisible(browser.findElement(selector)), TEST_TIMEOUT).then(function () {
                callback();
            });
        });
    }

    function login(callback) {
        browser.manage().deleteAllCookies();
        browser.get('https://' + app.fqdn);

        visible(by.id('loginButton'), function () {
            browser.findElement(by.id('username')).sendKeys(username);
            browser.findElement(by.id('passwordPlain')).sendKeys(password);
            browser.findElement(by.id('loginButton')).click();
            browser.wait(until.elementLocated(by.id('dropdown-configure')), TEST_TIMEOUT).then(function () { callback(); });
        });
    }

    function logout(callback) {
        var logout_btn = by.xpath('//ul[@class="dropdown-menu"]/li/a[@class="signout"]');

        browser.get('https://' + app.fqdn);

        visible(by.id('stream'), function () {
            browser.findElement(by.className('dropdown-toggle')).click();
            visible(logout_btn, function () {
                browser.findElement(logout_btn).click();

                browser.wait(until.elementLocated(by.id('loginButton')), TEST_TIMEOUT).then(function () { callback(); });
            });
        });
    }

    function addSubscription(callback) {
        var url = "https://cloudron.io/blog/rss.xml";

        browser.get('https://' + app.fqdn);

        visible(by.id('stream'), function () {
            browser.findElement(by.xpath('//a[@href=".?c=subscription"]')).click();
            visible(by.id('add_rss'), function () {
                browser.findElement(by.xpath('//input[@name="url_rss"]')).sendKeys(url);
                browser.findElement(by.xpath('//form[@id="add_rss"]/div/button[@type="submit"]')).click();
                visible(by.xpath('//div[@id="notification" and @class="notification good"]'), function() { callback(); });
            });
        });
    }

    function addUser(callback) {
        var test_username = 'test';
        var manage_users_btn = by.xpath('//a[@href=".?c=user&a=manage"]');

        browser.get('https://' + app.fqdn);

        visible(by.id('stream'), function () {
            browser.findElement(by.className('dropdown-toggle')).click();
            visible(manage_users_btn, function () {
                browser.findElement(manage_users_btn).click();
                visible(by.id('new_user_name'), function () {
                    browser.findElement(by.id('new_user_name')).sendKeys(test_username);
                    browser.findElement(by.id('new_user_passwordPlain')).sendKeys(password);
                    browser.findElement(by.xpath('//form[@action=".?c=user&a=create"]/div/div/button[@type="submit"]')).click();
                    visible(by.xpath('//div[@id="notification" and @class="notification good"]'), function() { callback(); });
                });
            });
        });
    }


    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --new --wait --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', function () {
        var inspect = JSON.parse(execSync('cloudron inspect'));

        app = inspect.apps.filter(function (a) { return a.location === LOCATION; })[0];

        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('can subscribe', addSubscription);
    it('can add users', addUser);
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can login', login);
    it('can logout', logout);

    it('move to different location', function () {
        browser.manage().deleteAllCookies();
        execSync('cloudron configure --location ' + LOCATION + '2', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    });

    it('can login', login);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

});
