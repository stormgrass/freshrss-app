FROM cloudron/base:0.10.0
MAINTAINER Rainloop Developers <support@cloudron.io>

EXPOSE 8000

RUN apt-get update \
    && apt-get -y install php php-curl php-gmp php-intl php-mbstring php-xml php-zip \
       libapache2-mod-php mysql-client php-mysql

ENV VERSION 1.6.3
ENV EXTENSIONS_VERSION eac4b749775c3e1b584c1a6b4a4487e4a4b8b159

RUN mkdir -p /app/code
WORKDIR /app/code

RUN wget "https://github.com/FreshRSS/FreshRSS/archive/${VERSION}.tar.gz" -O - \
    | tar -xz --strip-components=1

RUN mkdir -p /app/data \
    && mv data data-orig \
    && ln -s /app/data data

ADD change-ttrss-file-path.patch /app/code/change-ttrss-file-path.patch

RUN wget https://github.com/FreshRSS/Extensions/archive/${EXTENSIONS_VERSION}.tar.gz -O - \
    | tar -xz --strip-components=1 -C /app/code/extensions \
    && patch -p0 -d /app/code < /app/code/change-ttrss-file-path.patch \
    && mv /app/code/extensions /app/code/extensions-orig \
    && ln -s /app/data/extensions /app/code/extensions \
    && ln -s /app/data/extensions/ttrss.php /app/code/p/api/ttrss.php

ADD apache2.conf /etc/apache2/sites-available/freshrss.conf

RUN rm /etc/apache2/sites-enabled/* \
    && sed -e 's,^ErrorLog.*,ErrorLog "/dev/stderr",' -i /etc/apache2/apache2.conf \
    && sed -e "s,MaxSpareServers[^:].*,MaxSpareServers 5," -i /etc/apache2/mods-available/mpm_prefork.conf \
    && a2disconf other-vhosts-access-log \
    && echo "Listen 8000" > /etc/apache2/ports.conf \
    && a2enmod headers expires \
    && a2ensite freshrss

RUN rm -rf /var/lib/php \
    && ln -s /run/php /var/lib/php

ADD start.sh /app/code/start.sh

CMD [ "/app/code/start.sh" ]
